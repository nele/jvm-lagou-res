package com.example.report.controller;

import com.example.report.util.HttpClientUtil;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.concurrent.atomic.AtomicInteger;

@Controller
public class DemoController {
    AtomicInteger fast = new AtomicInteger();
    AtomicInteger slow = new AtomicInteger();

    @GetMapping("/fast")
    @ResponseBody
    public String fast() {
        String url = "http://www.baidu.com";
        String s = HttpClientUtil.executeGetRequestGBK(url, null);
        if (!StringUtils.isEmpty(s)) {
            fast.getAndIncrement();
        }
        return s;
    }

    @GetMapping("/slow")
    @ResponseBody
    public String slow() {
        String url = "http://google.com";
        String s = HttpClientUtil.executeGetRequestGBK(url, null);
        if (!StringUtils.isEmpty(s)) {
            slow.getAndIncrement();
        }
        return s;
    }

    @GetMapping("/stat")
    @ResponseBody
    public String stat() {
        return "fast" + fast.get() + ",slow:" + slow.get();
    }

}
