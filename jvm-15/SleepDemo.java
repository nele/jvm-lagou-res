public class SleepDemo {
    public static void main(String[] args) {
        new Thread(()->{
            try {
                Thread.sleep(Integer.MAX_VALUE);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        },"sleep-demo").start();
    }
}
