public class SynchronizedDemo {
	synchronized void m1() {
		System.out.println("m1");
	}
    static synchronized void  m2() {
		System.out.println("m2");
	}
	final Object lock = new Object();

	void doLock() {
		synchronized (lock) {
			System.out.println("lock");
		}
	}
}
